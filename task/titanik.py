import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    mr_median = df[df['Name'].str.contains('Mr.')]['Age'].median()
    mrs_median = df[df['Name'].str.contains('Mrs.')]['Age'].median()
    miss_median = df[df['Name'].str.contains('Miss.')]['Age'].median()

    # Find the number of missing values for each group
    mr_missing = df[df['Name'].str.contains('Mr.')]['Age'].isna().sum()
    mrs_missing = df[df['Name'].str.contains('Mrs.')]['Age'].isna().sum()
    miss_missing = df[df['Name'].str.contains('Miss.')]['Age'].isna().sum()

    # Fill missing values with the calculated medians
    df.loc[df['Name'].str.contains('Mr.'), 'Age'] = df.loc[df['Name'].str.contains('Mr.'), 'Age'].fillna(mr_median)
    df.loc[df['Name'].str.contains('Mrs.'), 'Age'] = df.loc[df['Name'].str.contains('Mrs.'), 'Age'].fillna(mrs_median)
    df.loc[df['Name'].str.contains('Miss.'), 'Age'] = df.loc[df['Name'].str.contains('Miss.'), 'Age'].fillna(miss_median)

    # Round the calculated medians to the nearest integer
    mr_median = round(mr_median)
    mrs_median = round(mrs_median)
    miss_median = round(miss_median)

    # Create the list of tuples in the required format
    result = [('Mr.', mr_missing, int(mr_median)),
              ('Mrs.', mrs_missing, int(mrs_median)),
              ('Miss.', miss_missing, int(miss_median))]

    return result

